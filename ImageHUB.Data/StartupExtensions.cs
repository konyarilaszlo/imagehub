﻿using System;
using ImageHUB.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ImageHUB.Data
{
    public static class StartupExtensions
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services, IConfiguration config)
        {

            Action<DbContextOptionsBuilder> optionsBuilder;
            var connectionString = config.GetConnectionString("DefaultConnection");

            optionsBuilder = options => options.UseSqlServer(connectionString);

            services.AddDbContextPool<ImageHUBContext>(options =>
            {
                optionsBuilder(options);
            });


            services.AddDefaultIdentity<ImageHUBIdentityUser>()
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<ImageHUBContext>();

            return services;
        }

        public static IServiceScope SeedData(this IServiceScope serviceScope)
        {

            var context = serviceScope.ServiceProvider.GetService<ImageHUBContext>();

            ImageHUBContext.SeedData(context);

            return serviceScope;

        }
    }
}
