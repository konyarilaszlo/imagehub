﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace ImageHUB.Data
{
    public class ImageHUBContext : IdentityDbContext
    {
        public ImageHUBContext(DbContextOptions<ImageHUBContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }

        public static void SeedData(ImageHUBContext context)
        {

            context.Database.Migrate();

        }
    }
}
