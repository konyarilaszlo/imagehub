﻿using ImageHUB.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ImageHUB.Configuration.Startup
{
    public static partial class ConfigurationExtensions
    {
        public static IServiceCollection ConfigureDatabase(this IServiceCollection services, IConfiguration config)
        {
            services.AddRepositories(config);
            return services;
        }

        public static IApplicationBuilder InitializeData(this IApplicationBuilder app, IConfiguration config)
        {
            var scope = app.ApplicationServices.CreateScope();

            var identityContext = scope.SeedData()
                .ServiceProvider.GetService<ImageHUBContext>();
            ImageHUBContext.SeedData(identityContext);

            return app;
        }
    }
}
