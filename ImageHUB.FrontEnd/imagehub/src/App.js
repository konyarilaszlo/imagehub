import React from 'react';
import './App.css';
import { ThemeProvider } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import CardMedia from '@material-ui/core/CardMedia';
import { CardActionArea } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import LoginPage from './LoginPage';

function LoginPageFunc(){
  return(
      <LoginPage/>
  );
}

function App() {
  return (
    <body>
    <div className="loginBackDiv">
    </div>
    <div>
     <ThemeProvider>
        <Card className="LoginCard">
          <CardActionArea>
          </CardActionArea>
          <CardMedia
          className="cardMediaLogo"
          title="ImageHub"
        />
        <CardContent>
          <Typography variant="body2"  component="p" className="loginText">
            Please log in with your Facebook account!
          </Typography>
        </CardContent>
          <CardActions>
            <Button className="signInButton"  size="small">Sign In!</Button> 
          </CardActions>
        </Card>
    </ThemeProvider>
    </div>
    </body>
  );
}

export default App;
